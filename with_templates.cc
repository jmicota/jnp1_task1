#include <iostream>
#include <string>
#include <regex>
#include <map>
#include <utility>
#include <cstdlib>
#include <algorithm>
#include <cctype>

using namespace std;

using long_int = long long;
using two_sums_type = pair<long_int, long_int>;
using line_type = pair<long_int, string>;
using line_container = map<string, line_type>;
using entry_info_container = map<string, pair<string, long_int>>;
using sum_container_car = map<string, two_sums_type>;
using sum_container_road = map<int, two_sums_type>;

#define HIGHWAY true
#define EXPRESSWAY false
#define MAX_ROAD_NAME_LENGTH 4
#define MIN_ROAD_NAME_LENGTH 2
#define MAX_CAR_NAME_LENGTH 11
#define MIN_CAR_NAME_LENGTH 3
#define MAX_NR_OF_ARGS_IN_LINE 3
#define MIN_DISTANCE_STRING_LENGTH 3
#define PRINT_CARS true
#define PRINT_ROADS false

bool correct_char(char c) {

    return (c == ',' || c == '?' || isspace(c) || isalnum(c));
}

bool correct_characters_in_line(string &text) {

    bool correct = true;
    size_t i = 0;

    while (correct && i < text.length()) {
        correct = correct_char(text[i]);
        i++;
    }

    return correct;
}

void print_line_error(line_type &line) {

    cerr << "Error in line " << line.first << ": " << line.second << '\n';
}

void reset_array(string array[3]) {

    array[0] = array[1] = array[2] = "";
}

two_sums_type make_new_sum() {

    return make_pair(-1, -1);
}

char extract_road_type(string &road) {

    return road[0];
}

bool is_highway(char road_type) {
    
    return (road_type == 'A');
}

bool correct_road_type(char road_type) {
    return (road_type == 'S' || road_type == 'A');
}

bool correct_road_names(const string &road1, const string &road2) {

    return (road1 == road2);
}

bool starts_with_questionmark(string &word) {

    return (!word.empty() && word[0] == '?');
}

string detach_first_char(string &str) {

    return str.substr(1, str.length());
}

// 6,0 -> 60
int get_int_value(string &text) {

    string clean_number = text.substr(0, text.length() - 2);
    clean_number += text[text.length() - 1];
    int number = stoi(clean_number);
    return number;
}

int extract_road_number(const string &road) {

    string number = road.substr(1, road.length());
    return stoi(number);
}

bool check_valid_car_name(string &car_str) {

    size_t car_str_length = car_str.length();

    if (car_str_length > MAX_CAR_NAME_LENGTH || 
        car_str_length < MIN_CAR_NAME_LENGTH)
        return false;

    for (size_t i = 0; i < car_str_length; i++) {
        if (!isalpha(car_str[i]) && !isdigit(car_str[i]))
            return false;
    }
    return true;
}

bool check_valid_distance_string(string &distance_str) {

    size_t word_length = distance_str.length();

    if (word_length < MIN_DISTANCE_STRING_LENGTH) {
        return false;
    }

    for (size_t i = 0; i < word_length - 2; i++) {
        if (!isdigit(distance_str[i]))
            return false;
    }

    if (distance_str[word_length - 2] != ',')
        return false;

    if (!isdigit(distance_str[word_length - 1]))
        return false;

    if (distance_str[0] == '0' && word_length > MIN_DISTANCE_STRING_LENGTH)
        return false;

    return true;
}

bool check_valid_road_name(string &str) {

    if (str.length() > MAX_ROAD_NAME_LENGTH ||
        str.length() < MIN_ROAD_NAME_LENGTH) {
        return false;
    }

    char road_type = extract_road_type(str);
    if (!correct_road_type(road_type)) {
        return false;
    }

    for (size_t i = 1; i < str.length(); i++) {

        if (!isdigit(str[i])) {
            return false;
        }
    }

    if (str[1] == '0') {
        return false;
    }

    return true;
}

bool two_args_in_question_line(string line_content[3]) {

    return (line_content[0].length() > 1 && !line_content[1].empty()) ||
           (line_content[0].length() == 1 && !line_content[2].empty());
}

bool check_args_of_car_entry(string line_content[3]) {

    return (check_valid_car_name(line_content[0]) &&
            check_valid_road_name(line_content[1]) &&
            check_valid_distance_string(line_content[2])); 
}

bool check_args_of_question(string line_content[3]) {

    if (line_content[0].empty()) {
        return false;
    }

    if (starts_with_questionmark(line_content[0]) &&
        two_args_in_question_line(line_content)) {

        return false;
    }

    if (line_content[0].length() > 1) {
        string key = detach_first_char(line_content[0]);
        return (check_valid_car_name(key) || 
                check_valid_road_name(key));
    }
    else if (!line_content[1].empty()) {
        string key = line_content[1];
        return (check_valid_car_name(key) || 
                check_valid_road_name(key));
    }

    return true;
}

bool split_into_words(string &line, string words[3]) {

    static const regex words_regex("[^\\s]+");
    auto words_begin = sregex_iterator(line.begin(), line.end(), words_regex);
    auto words_end = sregex_iterator();
    int which_word = 0;

    int how_many_words = distance(words_begin, words_end);
    if (how_many_words > MAX_NR_OF_ARGS_IN_LINE) {
        return false;
    }

    for (sregex_iterator i = words_begin; i != words_end; ++i) {
        smatch match = *i;
        string match_str = match.str();
        words[which_word] = match_str;
        which_word++;
    }

    return true;
}

void print_km_value(long_int kms) {

    if (kms == 0) {
        cout << "0,0";
    } else {
        cout << kms / 10 << "," << kms % 10;
    }
}

bool car_entered_road(const string &car, line_container &lines) {

    return !(lines.find(car) == lines.end());
}

template <typename T> 
bool already_exists_in_map(T key, map<T, two_sums_type> &sums) {

    return !(sums.find(key) == sums.end());
}

template <typename T> 
long_int get_distance(T &key, map<T, two_sums_type> &sums, bool highway) {

    if (already_exists_in_map(key, sums)) {
        if (highway) {
            return sums[key].first;
        }
        return sums[key].second;
    }
    else return -1;
}

template <typename T> 
void set_distance(T &key, long_int value, map<T, two_sums_type> &sums, bool highway) {

    if (highway) {
        sums[key].first = value;
    }
    else {
        sums[key].second = value;
    }
}

template <typename T> 
void print_one_car_info(T car, map<T, two_sums_type> &sums_of_kms) {

    long_int highway_distance = get_distance(car, sums_of_kms, HIGHWAY);
    long_int expressway_distance = get_distance(car, sums_of_kms, EXPRESSWAY);

    if (already_exists_in_map(car, sums_of_kms)) {
        cout << car;
        if (highway_distance != -1) {
            cout << " A ";
            print_km_value(highway_distance);
        }

        if (expressway_distance != -1) {
            cout << " S ";
            print_km_value(expressway_distance);
        }
        cout << "\n";
    }
}

template <typename T> 
void print_one_road_info(T road_nr, bool highway, bool expressway,
                          map<T, two_sums_type> &roads) {
    
    long_int highway_distance = get_distance(road_nr, roads, HIGHWAY);
    long_int expressway_distance = get_distance(road_nr, roads, EXPRESSWAY);

    if (highway && highway_distance != -1) {

        cout << "A" << road_nr << " ";
        print_km_value(highway_distance);
        cout << '\n';
    }
    
    if (expressway && expressway_distance != -1) {

        cout << "S" << road_nr << " ";
        print_km_value(expressway_distance);
        cout << '\n';
    }
}

void print_specific_road_info(string &road_name, sum_container_road &roads) {

    char road_type = extract_road_type(road_name);
    bool highway = is_highway(road_type);
    int road_number = extract_road_number(road_name);

    if (already_exists_in_map(road_number, roads)) {

        print_one_road_info(road_number, highway, !highway, roads);
    }
}

template <typename T> 
void print_all_map_info(map<T, two_sums_type> &sums, bool printing_cars) {

    auto it = sums.begin();
    while (it != sums.end()) {

        T key = it->first;
        if (printing_cars) {
            print_one_car_info(key, sums);
        }
        else {
            print_one_road_info(key, true, true, sums);
        }
        it++;
    }

}

void print_answer_to_specific_question(string &key, 
                                    sum_container_car &sums_of_kms, 
                                    sum_container_road &roads) {

    if (check_valid_car_name(key)) {
        print_one_car_info(key, sums_of_kms);
    }
    if (check_valid_road_name(key)) {
        print_specific_road_info(key, roads);
    }
}

void process_question(string words[3], sum_container_car &sums_of_kms, 
                      sum_container_road &roads) {

    if (words[0].length() > 1) {
        string key = detach_first_char(words[0]);
        print_answer_to_specific_question(key, sums_of_kms, roads);
    }
    else if (words[1].length() > 0) {
        string key = words[1];
        print_answer_to_specific_question(key, sums_of_kms, roads);
    }
    else {
        print_all_map_info(sums_of_kms, PRINT_CARS);
        print_all_map_info(roads, PRINT_ROADS);
    }
}

template <typename T>
void initiate_object(T &key, char road_type, map<T, two_sums_type> &sums) {

    if (!already_exists_in_map(key, sums)) {
        sums[key] = make_new_sum();
    }

    if (is_highway(road_type) && 
        get_distance(key, sums, HIGHWAY) == -1) {

        set_distance(key, 0, sums, HIGHWAY);

    } 
    
    if (!is_highway(road_type) && 
        get_distance(key, sums, EXPRESSWAY) == -1) {

        set_distance(key, 0, sums, EXPRESSWAY);
    }
}

template <typename T>
void add_value_to_map(int value, T &key, map<T, two_sums_type> &sums, 
                    bool to_first_in_pair) {
    
    if (to_first_in_pair) {
        sums[key].first += value;
    }
    else {
        sums[key].second += value;
    }
}

void add_distance(long_int distance, string &car, int road_nr, 
                char road_type, sum_container_road &roads, 
                sum_container_car &sums_of_kms) {

    initiate_object(road_nr, road_type, roads);
    initiate_object(car, road_type, sums_of_kms);

    add_value_to_map(distance, car, sums_of_kms, is_highway(road_type));
    add_value_to_map(distance, road_nr, roads, is_highway(road_type));
}

void fill_car_info(const string &car, const line_type &current_line, 
                    line_container &lines,
                    entry_info_container &entry_info, 
                    const string &road_name, long_int point_of_entry) {

    lines[car] = current_line;
    entry_info[car] = make_pair(road_name, point_of_entry);
}

void erase_car_info(const string &car, line_container &lines,
                    entry_info_container &entry_info) {

    entry_info.erase(car);
    lines.erase(car);
}

void process_car(string current_info[3], const line_type &current_line,
                 line_container &lines, sum_container_car &sums_of_kms,
                 entry_info_container &entry_info, sum_container_road &roads) {

    string car = current_info[0];
    string road_name = current_info[1];
    int road_nr = extract_road_number(road_name);
    char road_type = extract_road_type(road_name);
    long_int point_of_entry = get_int_value(current_info[2]);

    if (!car_entered_road(car, lines)) {

        fill_car_info(car, current_line, lines, entry_info, 
                      road_name, point_of_entry);
    } 
    else if (correct_road_names(road_name, entry_info[car].first)) {

        long_int distance = abs(point_of_entry - entry_info[car].second);
        add_distance(distance, car, road_nr, road_type, roads, sums_of_kms);
        erase_car_info(car, lines, entry_info);
    } 
    else {

        print_line_error(lines[car]);
        fill_car_info(car, current_line, lines, entry_info, 
                      road_name, point_of_entry);
    }
}

bool redirect_line(const line_type &line, string line_content[3],
                   line_container &lines, sum_container_car &kms,
                   entry_info_container &entry_info, sum_container_road &roads) {

    bool correct = true;

    if (starts_with_questionmark(line_content[0])) {

        correct = check_args_of_question(line_content);
        if (!correct) { return false; }
        process_question(line_content, kms, roads);
    }
    else {

        correct = check_args_of_car_entry(line_content);
        if (!correct) { return false; }
        process_car(line_content, line, lines, kms, entry_info, roads);
    }

    return true;
}

bool process_line(line_type &line, string line_content[3],
                  line_container &lines, sum_container_car &kms,
                  entry_info_container &entry_info, sum_container_road &roads) {

    size_t line_length = line.second.length();

    bool correct = correct_characters_in_line(line.second);
    if (!correct) { return false; }

    correct = split_into_words(line.second, line_content);
    if (!correct) { return false; }

    if (line_content[0].empty() && line_length > 0) {
        return false;
    }

    if (!line_content[0].empty()) {
        correct = redirect_line(line, line_content, lines, kms, entry_info, roads);
        if (!correct) { return false; }
    }

    return true;
}

int main() {

    ios_base::sync_with_stdio(0);
    string line;
    long_int nr_of_line = 1;
    line_container lines;
    sum_container_car kms;
    sum_container_road roads;
    entry_info_container entry_info;
    string words[3] = {"", "", ""};

    while (getline(cin, line)) {

        line_type paired_line = make_pair(nr_of_line, line);

        if (!process_line(paired_line, words, lines, kms, entry_info, roads))
            print_line_error(paired_line);
        
        reset_array(words);
        nr_of_line++;
    }

    return 0;
}