#include <iostream>
#include <string>
#include <regex>
#include <map>
#include <utility>
#include <cstdlib>
#include <algorithm>
#include <cctype>

using namespace std;

using long_int = long long;
using line_type = pair<long_int, string>;
using two_sums_type = pair<long_int, long_int>;
using sum_container_car = map<string, two_sums_type>;
using sum_container_road = map<int, two_sums_type>;
using line_container = map<string, line_type>;
using entry_info_container = map<string, pair<string, long_int>>;

#define HIGHWAY true
#define NOT_HIGHWAY false
#define MAX_ROAD_NAME_LENGTH 4
#define MIN_ROAD_NAME_LENGTH 2
#define MAX_CAR_NAME_LENGTH 11
#define MIN_CAR_NAME_LENGTH 3
#define MAX_NR_OF_ARGS_IN_LINE 3
#define MIN_DISTANCE_STRING_LENGTH 3

bool correct_char(char c) {

    return (c == ',' || c == '?' || isspace(c) || isalnum(c));
}

bool correct_line(string &text) {

    bool correct = true;
    size_t i = 0;

    while (correct && i < text.length()) {
        correct = correct_char(text[i]);
        i++;
    }

    return correct;
}

void reset_array(string array[3]) {

    array[0] = array[1] = array[2] = "";
}

bool check_valid_car_name(string &car) {

    bool valid = true;
    size_t car_name_length = car.length();

    if (car_name_length > MAX_CAR_NAME_LENGTH || 
        car_name_length < MIN_CAR_NAME_LENGTH)
        valid = false;

    for (size_t i = 0; i < car_name_length; i++) {

        if (!isalpha(car[i]) && !isdigit(car[i]))
            valid = false;
    }
    return valid;
}

bool check_valid_distance_string(string &distance) {

    bool valid = true;
    size_t word_length = distance.length();
    if (word_length < MIN_DISTANCE_STRING_LENGTH) {
        return false;
    }

    for (size_t i = 0; i < word_length - 2; i++) {

        if (!isdigit(distance[i]))
            valid = false;
    }

    if (distance[word_length - 2] != ',')
        valid = false;

    if (!isdigit(distance[word_length - 1]))
        valid = false;

    if (distance[0] == '0' && word_length > MIN_DISTANCE_STRING_LENGTH)
        valid = false;

    return valid;
}

char extract_road_type(string &road) {

    return road[0];
}

bool check_valid_road_name(string &str) {

    if (str.length() > MAX_ROAD_NAME_LENGTH ||
        str.length() < MIN_ROAD_NAME_LENGTH) {
        return false;
    }

    char road_type = extract_road_type(str);
    if (road_type != 'S' && road_type != 'A') {
        return false;
    }

    for (size_t i = 1; i < str.length(); i++) {

        if (!isdigit(str[i])) {
            return false;
        }
    }

    if (str[1] == '0') {
        return false;
    }

    return true;
}

bool starts_with_questionmark(string &word) {

    return (!word.empty() && word[0] == '?');
}

string detach_first_char(string &str) {

    return str.substr(1, str.length());
}

bool two_arguments_detected(string line_content[3]) {
    return (line_content[0].length() > 1 && !line_content[1].empty()) ||
           (line_content[0].length() == 1 && !line_content[2].empty());
}

bool check_args_of_car_entry(string line_content[3]) {
    
    return (check_valid_car_name(line_content[0]) &&
            check_valid_road_name(line_content[1]) &&
            check_valid_distance_string(line_content[2])); 
}

bool check_args_of_question(string line_content[3]) {

    if (line_content[0].empty()) {
        return false;
    }

    if (starts_with_questionmark(line_content[0]) &&
        two_arguments_detected(line_content)) {

        return false;
    }

    if (line_content[0].length() > 1) {
        string key = detach_first_char(line_content[0]);
        return (check_valid_car_name(key) || 
                check_valid_road_name(key));
    }
    else if (!line_content[1].empty()) {
        string key = line_content[1];
        return (check_valid_car_name(key) || 
                check_valid_road_name(key));
    }

    return true;
}

void print_line_error(line_type &line) {

    cerr << "Error in line " << line.first << ": " << line.second << '\n';
}

bool split_into_words(string &line, string words[3]) {

    static const regex words_regex("[^\\s]+");
    auto words_begin = sregex_iterator(line.begin(), line.end(), words_regex);
    auto words_end = sregex_iterator();
    int which_word = 0;

    int how_many_words = distance(words_begin, words_end);
    if (how_many_words > MAX_NR_OF_ARGS_IN_LINE) {
        return false;
    }

    for (sregex_iterator i = words_begin; i != words_end; ++i) {
        smatch match = *i;
        string match_str = match.str();
        words[which_word] = match_str;
        which_word++;
    }

    return true;
}

// 6,0 -> 60
int extract_int_value(string &text) {

    string clean_number = text.substr(0, text.length() - 2);
    clean_number += text[text.length() - 1];
    int number = stoi(clean_number);
    return number;
}

int extract_road_number(const string &road) {

    string number = road.substr(1, road.length());
    return stoi(number);
}

bool is_highway(char road_type) {
    
    return (road_type == 'A');
}

bool car_entered_road(const string &car, line_container &lines) {

    return !(lines.find(car) == lines.end());
}

bool car_already_seen(const string& key, sum_container_car &sums) {

    return !(sums.find(key) == sums.end());
}

bool road_already_seen(int key, sum_container_road &sums) {

    return !(sums.find(key) == sums.end());
}

long_int get_car_distance(const string &key, sum_container_car &sums, bool highway) {

    if (highway) {
        return sums[key].first;
    }
    return sums[key].second;
}

long_int get_road_distance(int key, sum_container_road &sums, bool highway) {

    if (highway) {
        return sums[key].first;
    }
    return sums[key].second;
}

void set_car_distance(const string &key, long_int value, 
                    sum_container_car &sums, bool highway) {

    if (highway) {
        sums[key].first = value;
    }
    else {
        sums[key].second = value;
    }
    
}

void set_road_distance(int key, long_int value, 
                        sum_container_road &sums, bool highway) {

    if (highway) {
        sums[key].first = value;
    }
    else {
        sums[key].second = value;
    }
}

void print_km_value(long_int kms) {

    if (kms == 0) {
        cout << "0,0";
    } else {
        cout << kms / 10 << "," << kms % 10;
    }
}

void print_car_distances(const string &car, sum_container_car &sums_of_kms) {

    bool highway = true;
    long_int highway_distance = get_car_distance(car, sums_of_kms, highway);
    long_int expressway_distance = get_car_distance(car, sums_of_kms, !highway);

    if (highway_distance != -1) {
        
        cout << " A ";
        print_km_value(highway_distance);
    }

    if (expressway_distance != -1) {
        
        cout << " S ";
        print_km_value(expressway_distance);
    }
}

void print_one_car_info(string &car, sum_container_car &sums_of_kms) {

    if (car_already_seen(car, sums_of_kms)) {

        cout << car;
        print_car_distances(car, sums_of_kms);
        cout << "\n";
    }
}

void print_all_car_info(sum_container_car &sums_of_kms) {

    auto it = sums_of_kms.begin();

    while (it != sums_of_kms.end()) {

        string key = it->first;
        print_one_car_info(key, sums_of_kms);
        it++;
    }

}

void print_full_road_info(int road_nr, bool highway, bool expressway,
                          sum_container_road &roads) {
    
    long_int highway_distance = get_road_distance(road_nr, roads, true);
    long_int expressway_distance = get_road_distance(road_nr, roads, false);

    if (highway && highway_distance != -1) {

        cout << "A" << road_nr << " ";
        print_km_value(highway_distance);
        cout << '\n';
    }
    
    if (expressway && expressway_distance != -1) {

        cout << "S" << road_nr << " ";
        print_km_value(expressway_distance);
        cout << '\n';
    }
}

void print_specific_road_info(string &road_name, sum_container_road &roads) {

    char road_type = extract_road_type(road_name);
    bool highway = is_highway(road_type);
    int road_number = extract_road_number(road_name);

    if (road_already_seen(road_number, roads)) {

        print_full_road_info(road_number, highway, !highway, roads);
    }
}

void print_all_road_info(sum_container_road &roads) {

    auto it = roads.begin();

    while (it != roads.end()) {

        int key = it->first;
        print_full_road_info(key, true, true, roads);
        
        it++;
    }
}

two_sums_type make_new_sum() {

    return make_pair(-1, -1);
}

void initiate_car(const string &key, char road_type, sum_container_car &sums) {

    bool highway = true;
    if (!car_already_seen(key, sums)) {

        sums[key] = make_new_sum();
    }

    if (is_highway(road_type) && 
        get_car_distance(key, sums, highway) == -1) {

        set_car_distance(key, 0, sums, highway);

    } 
    
    if (!is_highway(road_type) && 
        get_car_distance(key, sums, !highway) == -1) {

        set_car_distance(key, 0, sums, !highway);
    }
}

void initiate_road(int key, char road_type, sum_container_road &sums) {

    bool highway = true;
    if (!road_already_seen(key, sums)) {

        sums[key] = make_new_sum();
    }

    if (is_highway(road_type) && 
        get_road_distance(key, sums, highway) == -1) {

        set_road_distance(key, 0, sums, highway);

    } 
    
    if (!is_highway(road_type) && 
        get_road_distance(key, sums, !highway) == -1) {

        set_road_distance(key, 0, sums, !highway);
    }
}

bool correct_road_names(const string &road1, const string &road2) {

    return (road1 == road2);
}

void add_value_to_car_map(int value, const string &key, 
                        sum_container_car &sums, 
                        bool to_first_in_pair) {
    
    if (to_first_in_pair) {
        sums[key].first += value;
    }
    else {
        sums[key].second += value;
    }
}

void add_value_to_road_map(long_int value, int key, 
                        sum_container_road &sums, 
                        bool to_first_in_pair) {
    
    if (to_first_in_pair) {
        sums[key].first += value;
    }
    else {
        sums[key].second += value;
    }
}

void add_distance(long_int distance, const string &car, int road_nr, 
                char road_type, sum_container_road &roads, 
                sum_container_car &sums_of_kms) {

    initiate_road(road_nr, road_type, roads);
    initiate_car(car, road_type, sums_of_kms);

    add_value_to_car_map(distance, car, sums_of_kms, is_highway(road_type));
    add_value_to_road_map(distance, road_nr, roads, is_highway(road_type));
}

void process_question(string words[3], sum_container_car &sums_of_kms, 
                      sum_container_road &roads) {

    if (words[0].length() > 1) {

        string key = detach_first_char(words[0]);
        print_one_car_info(key, sums_of_kms);
        if (check_valid_road_name(key)) {
            print_specific_road_info(key, roads);
        }
    } 
    else if (words[1].length() > 0) {

        string key = words[1];
        print_one_car_info(key, sums_of_kms);
        if (check_valid_road_name(key)) {
            print_specific_road_info(key, roads);
        }
    } 
    else {

        print_all_car_info(sums_of_kms);
        print_all_road_info(roads);
    }
}

void fill_car_info(const string &car, const line_type &current_line, 
                    line_container &lines,
                    entry_info_container &entry_info, 
                    const string &road_name, long_int point_of_entry) {

    lines[car] = current_line;
    entry_info[car] = make_pair(road_name, point_of_entry);
}

void erase_car_info(const string &car, line_container &lines,
                    entry_info_container &entry_info) {

    entry_info.erase(car);
    lines.erase(car);
}

void process_car(string current_info[3], const line_type &current_line,
                 line_container &lines, sum_container_car &sums_of_kms,
                 entry_info_container &entry_info, sum_container_road &roads) {

    string car = current_info[0];
    string road_name = current_info[1];
    int road_nr = extract_road_number(road_name);
    char road_type = extract_road_type(road_name);
    long_int point_of_entry = extract_int_value(current_info[2]);

    if (!car_entered_road(car, lines)) {

        fill_car_info(car, current_line, lines, entry_info, 
                      road_name, point_of_entry);
    } 
    else if (correct_road_names(road_name, entry_info[car].first)) {

        long_int distance = abs(point_of_entry - entry_info[car].second);
        add_distance(distance, car, road_nr, road_type, roads, sums_of_kms);
        erase_car_info(car, lines, entry_info);
    } 
    else {

        print_line_error(lines[car]);
        fill_car_info(car, current_line, lines, entry_info, 
                      road_name, point_of_entry);
    }
}

bool redirect_line(const line_type &line, string line_content[3],
                   line_container &lines, sum_container_car &kms,
                   entry_info_container &entry_info, sum_container_road &roads) {

    bool correct = true;

    if (starts_with_questionmark(line_content[0])) {

        correct = check_args_of_question(line_content);
        if (!correct) { return false; }
        process_question(line_content, kms, roads);
    }
    else {

        correct = check_args_of_car_entry(line_content);
        if (!correct) { return false; }
        process_car(line_content, line, lines, kms, entry_info, roads);
    }

    return true;
}

bool process_line(line_type &line, string line_content[3],
                  line_container &lines, sum_container_car &kms,
                  entry_info_container &entry_info, sum_container_road &roads) {

    size_t line_length = line.second.length();
    bool correct = correct_line(line.second);
    if (!correct) { return false; }

    correct = split_into_words(line.second, line_content);
    if (!correct) { return false; }

    if (line_content[0].empty() && line_length > 0) {
        return false;
    }

    if (!line_content[0].empty()) {
        correct = redirect_line(line, line_content, lines, kms, entry_info, roads);
        if (!correct) { return false; }
    }

    return true;
}

int main() {

    string line;
    long_int nr_of_line = 1;

    line_container lines;
    sum_container_car kms;
    sum_container_road roads;
    entry_info_container entry_info;
    string words[3] = {"", "", ""};

    while (getline(cin, line)) {

        line_type paired_line = make_pair(nr_of_line, line);

        if (!process_line(paired_line, words, lines, kms, entry_info, roads)) {

            print_line_error(paired_line);
        }

        reset_array(words);
        nr_of_line++;
    }

    return 0;
}